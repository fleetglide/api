# skyglide-npm
skyglide.com API access for node

## Status
[![stability](https://img.shields.io/badge/stablilty-alpha-read.svg)](https://nodejs.org/dist/latest-v6.x/docs/api/)
[![build status](https://gitlab.com/skyglide/skyglide/badges/master/build.svg)](https://gitlab.com/skyglide/skyglide/commits/master)
[![node](https://img.shields.io/badge/node->=%206.x.x-blue.svg)](https://nodejs.org/dist/latest-v6.x/docs/api/)

## What is Skyglide?
Skyglide is a wifi mesh network service by Lossless GmbH.
It is designed  to prevent single points of failure.
It allows failover redundency with multiple internet providers.
skyglide.com is a perfect fit for local on site networks that require reliabe HD AV streaming to multiple subscribers.

### Cloud Configuration
Skyglide.com access points are managed through the skyglide.com cloud dashboard.
You can devide them into multiple networks or unify them as one.
A network can consist of up to 100 Access points currently.
Access points can span a local network around the globe.
That means any Access Point does need an internet connection either through the LAN port
or wirelessly through another local mesh device.
However when connected to the internet it doesn't matter where the access point is located.
This allows global companies to have one VPN spanning across all their locations world wide.

## About this library
This library (once finished) will allow you to easily talk to the skyglide network from node
to integrate access point behaviour with existing applications (e.g. LDAP sync for intranet resource access).

## Skyglide bundles
a medium skyglide.com service plan will be bundled at no additional cost with

* any enterprise bellini.io music streaming packages.
* any enterprise mojo.cloud static hosting packages.

## About the authors:
[![Project Phase](https://mediaserve.lossless.digital/lossless.com/img/createdby_github.svg)](https://lossless.com/)